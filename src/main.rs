extern crate reqwest;
extern crate select;
#[macro_use]
extern crate lazy_static;
extern crate csv;
extern crate regex;
extern crate serde;
extern crate serde_derive;

use chrono::prelude::*;
use regex::Regex;
use select::document::Document;
use select::predicate::Class;
use std::collections::HashMap;
use std::time::Duration;
use std::time::Instant;

#[derive(Clone)]
struct User {
    msg_tot: u64,
    weekdays_tot: u64,
    weekends_tot: u64,
    working_hours_tot: u64,
    after_work_tot: u64,
    quote_tot: u64,
    mondays: u64,
    tuesdays: u64,
    wednesdays: u64,
    thursdays: u64,
    fridays: u64,
    saturdays: u64,
    sundays: u64,
    troll_ratio: f64,
}

impl User {
    fn new() -> User {
        User {
            msg_tot: 0,
            weekdays_tot: 0,
            weekends_tot: 0,
            working_hours_tot: 0,
            after_work_tot: 0,
            quote_tot: 0,
            mondays: 0,
            tuesdays: 0,
            wednesdays: 0,
            thursdays: 0,
            fridays: 0,
            saturdays: 0,
            sundays: 0,
            troll_ratio: 0.0,
        }
    }
    fn reset(&mut self) {
        self.msg_tot = 0;
        self.weekdays_tot = 0;
        self.weekends_tot = 0;
        self.working_hours_tot = 0;
        self.after_work_tot = 0;
        self.quote_tot = 0;
        self.mondays = 0;
        self.tuesdays = 0;
        self.wednesdays = 0;
        self.thursdays = 0;
        self.fridays = 0;
        self.saturdays = 0;
        self.sundays = 0;
        self.troll_ratio = 0.0;
    }
    fn incr_msg_tot(&mut self) {
        self.msg_tot += 1;
    }
    fn incr_weekends_tot(&mut self) {
        self.weekends_tot += 1;
    }
    fn incr_weekdays_tot(&mut self) {
        self.weekdays_tot += 1;
    }
    fn incr_quote_tot(&mut self, tot: u64) {
        self.quote_tot += tot;
    }
    fn incr_mondays(&mut self) {
        self.mondays += 1;
    }
    fn incr_tuesdays(&mut self) {
        self.tuesdays += 1;
    }
    fn incr_wednesdays(&mut self) {
        self.wednesdays += 1;
    }
    fn incr_thursdays(&mut self) {
        self.thursdays += 1;
    }
    fn incr_fridays(&mut self) {
        self.fridays += 1;
    }
    fn incr_saturdays(&mut self) {
        self.saturdays += 1;
    }
    fn incr_sundays(&mut self) {
        self.sundays += 1;
    }
    fn incr_working_hours(&mut self) {
        self.working_hours_tot += 1;
    }
    fn incr_after_work(&mut self) {
        self.after_work_tot += 1;
    }
}

fn main() -> Result<(), Box<std::error::Error>> {
    let mut users: HashMap<String, User> = HashMap::new();

    let url_beginning =
        "https://forum.hardware.fr/forum2.php?config=hfr.inc&cat=13&subcat=423&post=21512&page=";
    let mut url_page_number: u64 = 706;
    let url_end = "&p=1&sondage=0&owntopic=0&trash=0&trash_post=0&print=1&numreponse=0&quote_only=0&new=0&nojs=0";

    let client = reqwest::Client::builder()
        .timeout(Duration::from_secs(30))
        .build()?;
    let mut user_tmp = User::new();

    lazy_static! {
        static ref DATE_REGEX: Regex =
            Regex::new(r"(?P<d>\d{2})\-(?P<m>\d{2})\-(?P<y>\d{4})").unwrap();
        static ref TIME_REGEX: Regex = Regex::new(r"(\d{2}):(\d{2}):(\d{2})").unwrap();
        static ref QUOTE_REGEX: Regex = Regex::new(r"Message cité \d{1,4} fois").unwrap();
    }

    let mut last_page = false;
    let mut reached_end_date = false;

    let start_date = Utc.ymd(2019, 1, 1);
    let end_date = Utc.ymd(2019, 06, 30);

    let now = Instant::now();
    while (!last_page) && (!reached_end_date) {
        println!("{}", url_page_number);

        let url = url_beginning.to_string() + &url_page_number.to_string() + &url_end.to_string();

        let res = client.get(&url).send()?;

        // if res.status().is_success() {
        if let Ok(document) = Document::from_read(res) {

            url_page_number += 1;

            // stop condition for the while loop. If we reached the last page then we set the flag to true
            for node in document.find(Class("pagepresuiv")) {
                if let Some(x) = node.find(Class("cFondu")).next() {
                    if x.text() == "Page Suivante" {
                        last_page = true;
                    }
                }
            }

            // go through each message on the page
            for node in document.find(Class("messagetable")) {
                user_tmp.reset();

                // get the username, we break from the loop if there aren't any
                let username: String;
                match node.find(Class("s2")).next() {
                    Some(x) => username = x.text(),
                    None => break,
                }

                // get the date when the message was posted
                let date_and_time = node
                    .find(Class("left"))
                    .next()
                    .map(|date_and_time| date_and_time.text())
                    .expect("there's nothing");
                // grab date yyyy-mm-dd
                let date: &str;
                match DATE_REGEX.captures(&date_and_time) {
                    Some(x) => date = x.get(0).map_or("", |m| m.as_str()),
                    None => break,
                }

                // grab time hh:mm:ss
                let time: &str;
                match TIME_REGEX.captures(&date_and_time) {
                    Some(x) => time = x.get(0).map_or("", |m| m.as_str()),
                    None => break,
                }

                // convert year, month, day, hour, minute, second to u32
                let mut date_iter = date.split('-');
                let day = date_iter.next().unwrap().parse::<u32>().unwrap();
                let month = date_iter.next().unwrap().parse::<u32>().unwrap();
                let year = date_iter.next().unwrap().parse::<i32>().unwrap();
                let mut time_iter = time.split(':');
                let hour = time_iter.next().unwrap().parse::<u32>().unwrap();

                // check we are within start_date < msg_date > end_date
                // if not then we skip this message
                let post_date = Utc.ymd(year, month, day);
                if post_date >= start_date && post_date <= end_date {
                    // save already existing user's info into the user_tmp's fields
                    if let Some(x) = users.get(&username) {
                        user_tmp = x.clone();
                    }

                    // get number of times the message has been quoted
                    if let Some(x) = node.find(Class("cLink")).next() {
                        if let Some(v) = QUOTE_REGEX.captures(&x.text()) {
                            let mut quote_iter = v.get(0).map_or("", |m| m.as_str()).split(' ');
                            let quote_number = quote_iter.nth(2).unwrap().parse::<u64>().unwrap();
                            user_tmp.incr_quote_tot(quote_number);
                        }
                    }

                    // incr user's tots based on the day the message was posted
                    let dt = Utc.ymd(year, month, day).weekday();
                    match dt {
                        Weekday::Mon => {
                            user_tmp.incr_mondays();
                            user_tmp.incr_weekdays_tot();
                        }
                        Weekday::Tue => {
                            user_tmp.incr_tuesdays();
                            user_tmp.incr_weekdays_tot();
                        }
                        Weekday::Wed => {
                            user_tmp.incr_wednesdays();
                            user_tmp.incr_weekdays_tot();
                        }
                        Weekday::Thu => {
                            user_tmp.incr_thursdays();
                            user_tmp.incr_weekdays_tot();
                        }
                        Weekday::Fri => {
                            user_tmp.incr_fridays();
                            user_tmp.incr_weekdays_tot();
                        }
                        Weekday::Sat => {
                            user_tmp.incr_saturdays();
                            user_tmp.incr_weekends_tot();
                        }
                        Weekday::Sun => {
                            user_tmp.incr_sundays();
                            user_tmp.incr_weekends_tot();
                        }
                    }

                    // users posting during weekdays and between 9am to 7pm
                    match hour {
                        9...19 => match dt {
                            Weekday::Mon
                            | Weekday::Tue
                            | Weekday::Wed
                            | Weekday::Thu
                            | Weekday::Fri => user_tmp.incr_working_hours(),
                            _ => {}
                        },
                        _ => user_tmp.incr_after_work(),
                    }

                    // total msg
                    user_tmp.incr_msg_tot();

                    // update user's fields
                    users.insert(username, user_tmp.clone());
                }

                if post_date > end_date {
                    // if post date posterior in time to the end date then set the flag that will break the loop
                    reached_end_date = true;
                }
            }
        } else {
            // println!("Something happened. Status: {:?}", res.status());
            println!("Couldn't get the page, retrying...");
        }
    }

    println!("Elapsed  time: {}s", now.elapsed().as_secs());

    let mut users_data_out = csv::Writer::from_path("users_data_out.txt")?;
    users_data_out.write_record(&["Username", "Total msg", "Weekdays", "Weekends", "Working hours", "After work", "Quoted", "Mondays", "Tuesdays", "Wednesdays", "Thurdays", "Fridays", "Saturdays", "Sundays", "Troll ratio"])?;

    users_data_out.flush()?;

    Ok(())
}
