import matplotlib.pyplot as plt
import csv
from itertools import islice

def autolabel(rects, xpos='center', angle=0):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """

    xpos = xpos.lower()  # normalize the case of the parameter
    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0.5, 'right': 0.57, 'left': 0.43}  # x_txt = x + w*off

    for rect in rects:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()*offset[xpos], 1.01*height,
        '{}'.format(height), ha=ha[xpos], va='bottom', rotation=angle)

last_user_rank = 40

# Top posters
top_posters_x = []
top_posters_y = []
top_posters_and_working_hours_y = []
top_posters_and_after_work_y = []
with open('users_data_out.txt','r', encoding="utf-8") as csvfile:
    csv_top_posters = csv.reader(csvfile, delimiter=',')
    next(csv_top_posters, None)
    sorted_top_posters = sorted(csv_top_posters, key=lambda row:int(row[1]), reverse=True)
    for row in islice(sorted_top_posters, 0, last_user_rank):
        top_posters_x.append(row[0])
        top_posters_y.append(int(row[1]))
        top_posters_and_working_hours_y.append(int(row[4]))
        top_posters_and_after_work_y.append(int(row[5]))

# Top posters during working hours
top_working_hours_x = []
top_working_hours_y = []
with open('users_data_out.txt','r', encoding="utf-8") as csvfile:
    csv_working_hours = csv.reader(csvfile, delimiter=',')
    next(csv_working_hours, None)
    sorted_top_working_hours = sorted(csv_working_hours, key=lambda row:int(row[4]), reverse=True)
    for row in islice(sorted_top_working_hours, 0, last_user_rank):
        top_working_hours_x.append(row[0])
        top_working_hours_y.append(int(row[4]))

# Top troll ratio
top_troll_ratio_x = []
top_troll_ratio_y = []
min_msg = 70
with open('users_data_out.txt','r', encoding="utf-8") as csvfile:
    csv_troll_ratio = csv.reader(csvfile, delimiter=',')
    next(csv_troll_ratio, None)
    sorted_top_troll_ratio = sorted(csv_troll_ratio, key=lambda row:float(row[14]), reverse=True)
    i = 0
    for row in sorted_top_troll_ratio:
        if(int(row[1]) >= min_msg):
            top_troll_ratio_x.append(row[0])
            top_troll_ratio_y.append(float(row[14]))
            i += 1
        if(i >= last_user_rank):
            break

# Most quoted users
top_quoted_x = []
top_quoted_y = []
with open('users_data_out.txt','r', encoding="utf-8") as csvfile:
    csv_quoted = csv.reader(csvfile, delimiter=',')
    next(csv_quoted, None)
    sorted_quoted = sorted(csv_quoted, key=lambda row:int(row[6]), reverse=True)
    for row in islice(sorted_quoted, 0, last_user_rank):
        top_quoted_x.append(row[0])
        top_quoted_y.append(int(row[6]))

plt.figure(1)
top_posters_graph = plt.bar(top_posters_x,top_posters_y, color='c', label='Weekend')
plt.bar(top_posters_x,top_posters_and_working_hours_y, color=('#3366ff'), label='Lun-Ven (9h->19h)')
plt.bar(top_posters_x,top_posters_and_after_work_y, bottom=top_posters_and_working_hours_y, color=('#ffcc00'), label='Lun-Ven (19h->9h)')
plt.xlabel('Username')
plt.ylabel('Messages')
plt.xticks(rotation=45, ha='right')
title = "Top " + str(last_user_rank) + " des posteurs 2019 (01/01 - 30/06)"
plt.title(title)
plt.legend()
autolabel(top_posters_graph, "center", 90)

plt.figure(2)
top_overpaid = plt.bar(top_working_hours_x,top_working_hours_y, color=('#3366ff'))
plt.xlabel('Username')
plt.ylabel('Messages')
title_working_hours = "Top " + str(last_user_rank) + " des posteurs trop payés 2019 (01/01 - 30/06)"
plt.title(title_working_hours)
plt.xticks(rotation=45, ha='right')
autolabel(top_overpaid, "center", 90)

plt.figure(3)
troll_ratio_graph = plt.bar(top_troll_ratio_x, top_troll_ratio_y, color=('#9966ff'))
title_troll_ratio = "Top " + str(last_user_rank) + " troll ratio 2019 (01/01 - 30/06)"
plt.xlabel('Username')
plt.ylabel('Messages')
plt.title(title_troll_ratio)
plt.xticks(rotation=45, ha='right')
autolabel(troll_ratio_graph, "center")

plt.figure(4)
top_quoted_graph = plt.bar(top_quoted_x, top_quoted_y, color=('#39ac39'))
plt.xlabel('Username')
plt.ylabel('Messages')
title_top_quoted = "Top " + str(last_user_rank) + " des posteurs les plus quotés 2019 (01/01 - 30/06)"
plt.title(title_top_quoted)
autolabel(top_quoted_graph, "center")
plt.xticks(rotation=45, ha='right')

plt.show()
